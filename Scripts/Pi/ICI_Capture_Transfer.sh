#!/bin/sh
echo Capturing Image ...
python3 /home/pi/Desktop/Python_Programs/FLIR_Camera_Grab_V1.py

echo Done

year=`date '+%Y'`;
month=`date '+%m'`;
day=`date '+%d'`;
time=`date '+%H_%M_%S'`;
filepath="/media/icidatabase/ICI_Image/y$year/m$month/d$day/"
file_to_copy="/home/pi/Desktop/last_image/FLIR_Capture.p"

echo copying $file_to_copy ...
ssh icidatabase@ee-database "mkdir -p $filepath"
scp $file_to_copy icidatabase@ee-database:$filepath
ssh icidatabase@ee-database mv "${filepath}FLIR_Capture.p" "${filepath}${time}.p"
echo Done
