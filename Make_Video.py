import sys
from datetime import date
def mark_video(image_folder,video_name):
    import cv2
    import os
    import numpy as np
    import pickle
    fps = 32
    cc = cv2.VideoWriter_fourcc(*'XVID')
    #read in pickle format
    imagesPickle = [img for img in os.listdir(image_folder) if img.endswith(".p")]
    #sort the order
    imagesPickle.sort(key=lambda fname: int(fname.replace('_','').split('.')[0]))

    #read in a frame
    img=pickle.load(open(os.path.join(image_folder, imagesPickle[0]),"rb"))
    #normalize
    img = 255*(img - img.min())/(img.max()-img.min())
    img=img.astype(np.uint8)
    height, width = img.shape
    #create frame
    video = cv2.VideoWriter(video_name, cc, fps, (width,height))
    #read in all the frames
    for IP in imagesPickle:
        try:
            im_gray=pickle.load(open(os.path.join(image_folder, IP),"rb"))
        except:
            print(IP)
        im_gray = 255*(im_gray - im_gray.min())/(im_gray.max()-im_gray.min())
        im_gray=im_gray.astype(np.uint8)
        im_color = cv2.cvtColor(im_gray, cv2.COLOR_GRAY2RGB)
        im_color = cv2.applyColorMap(im_color, cv2.COLORMAP_JET)
        #print(im_color.shape)
        video.write(im_color)
    #cv2.destroyAllWindows()
    video.release()

from datetime import date
from datetime import timedelta

yesterday = date.today() - timedelta(days = 1)
print()
print(yesterday)
print()
year=yesterday.year
month=f'{yesterday.month:{0}{2}}'
day=f'{yesterday.day:{0}{2}}'

file_path = '/media/icidatabase/ICI_Image/y{year}/m{month}/d{day}/'.format(year=year,month=month,day=day)
video_name = '/media/icidatabase/Videos/y{year}m{month}d{day}Recap.avi'.format(year=year,month=month,day=day)
print()
print(file_path)
print(video_name)
mark_video(file_path,video_name)
