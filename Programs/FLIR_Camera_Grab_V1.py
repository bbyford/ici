# All necessary imports
# sys used for the system
# cv2 is used for openCV commands
# flirpy allows us to use camera with pi
# numpy used for handling size
from flirpy.camera.boson import Boson
import numpy as np
import pickle
# Boson Photo Capture

with Boson() as camera:
        # Activate Camera and Takes Photo
        #print("taking Image")
        img = camera.grab().astype(np.float32)
        pickle.dump(img,open('/home/pi/Desktop/last_image/FLIR_Capture.p',"wb"))
print("done");
# Programmer Notes and Suggestions: This is a very basic, take photo, adjust it to colormap and save it.
# You'll want to call upon this program remotely if you can through a loop in another program.
